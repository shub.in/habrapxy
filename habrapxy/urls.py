from django.urls import re_path

from .views import auth, home, resource

urlpatterns = [
    re_path(r'^viewcount/.*', resource, name='viewcount'),
    re_path(r'^fonts/.*', resource, name='fonts'),
    re_path(r'^auth/.*', auth, name='auth'),
    re_path(r'^.*', home, name='home'),
]
