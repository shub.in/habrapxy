import re
from urllib.request import urlopen

from bs4 import BeautifulSoup

from django.shortcuts import HttpResponse, redirect

URL = 'https://habrahabr.ru'


def home(request):
    pattern = re.compile(r'\b\w{6}\b')
    url = '{}{}'.format(URL, request.get_full_path())
    soup = BeautifulSoup(urlopen(url).read(), 'html5lib')
    for link in soup.find_all('a'):
        if link.get('href') and URL in link.get('href'):
            link['href'] = link['href'].replace(URL, '')
    for txt in soup.find_all(text=True):
        if txt.strip() and txt.parent.name not in ['script', 'style']:
            words = list(set(pattern.findall(txt)))
            for word in words:
                tm = txt.replace(word, word+'™')
                if txt.parent:
                    txt.replace_with(tm)
    html = soup.prettify()
    return HttpResponse(html)


def resource(request):
    url = '{}{}'.format(URL, request.get_full_path())
    return HttpResponse(urlopen(url).read())


def auth(request):
    url = '{}{}'.format(URL, request.get_full_path())
    return redirect(url)
